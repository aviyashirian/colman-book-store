﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using final_project.Models;
using final_project.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace final_project.Controllers
{
    public class AdminController : Controller
    {
        public class CostumerViewModel
        {
            public string FullName { get; set; }
            public string Email { get; set; }
            public int OrdersNumber { get; set; }
        }

        private readonly BookStoreContext _context;
        private readonly IWebHostEnvironment hostingEnvironment;

        public AdminController(BookStoreContext context, IWebHostEnvironment env)
        {
            _context = context;
            hostingEnvironment = env;
        }

        #region Orders

        public IActionResult Orders()
        {
            if(HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            List<Order> orders = _context.Orders.Include("Costumer").ToList();
            List<OrderStatus> statuses = _context.OrderStatuses.ToList();
            ViewBag.Orders = orders;
            ViewBag.statuses = statuses;

            return View();
        }

        public IActionResult UpdateOrderStatus(int  orderId, string id)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            try
            {
                List<Order> orders = _context.Orders.Include("Costumer").ToList();
                Order orderToEdit = orders.Single(Order => Order.Id == orderId);

                OrderStatus orderStatus = _context.OrderStatuses.Single(s => s.ID == int.Parse(id));
                orderToEdit.Status = orderStatus;

                _context.Update(orderToEdit);
                _context.SaveChanges();

                return Redirect("/Admin/Orders");
            }
            catch(Exception)
            {
                return Redirect("/Admin/Orders");
            }
        }

        [HttpPost]
        public IActionResult Orders(int orderId, int orderStatus, DateTime? orderDate)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            List<Order> orders;

            if (orderId != 0)
            {
                orders = _context.Orders.Where((order) => order.Id == orderId).Include("Costumer").ToList();
            }
            else if(orderId == 0 && orderStatus == 0 && orderDate == null)
            {
                orders = _context.Orders.Include("Costumer").ToList();
            }
            else {
                orders = _context.Orders.Where((order) => (orderStatus != 0 && orderDate != null && orderStatus == order.Status.ID) ||
                        (orderStatus != 0 && orderStatus == order.Status.ID) ||
                        (orderDate != null && orderDate.Equals(order.OrderDate))).Include("Costumer").ToList();
            }

            List<OrderStatus> statuses = _context.OrderStatuses.ToList();

            ViewBag.Orders = orders;
            ViewBag.statuses = statuses;

            return View();
        }

        #endregion

        #region Categories

        public IActionResult Categories()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            List<Category> categories = _context.Categories.Where(cat => !cat.IsDeleted).ToList();
            ViewBag.Categories = categories;
            return View();
        }

        public IActionResult RemoveCategory(int id)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            if (_context.Products.Where(product => !product.IsDeleted && product.Category.ID == id).Count() == 0)
            {
                _context.Categories.Single(p => p.ID == id).IsDeleted = true;
                _context.SaveChanges();
            }
            else
                TempData["CategoryRemovalFailed"] = true;

            return Redirect("/Admin/Categories");
        }

        public IActionResult AddCategory(string name)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            if(name == null)
            {
                return Redirect("/Admin/Categories");
            }

            Category newCategory = new Category() { Name = name };

            _context.Add(newCategory);
            _context.SaveChanges();
            return Redirect("/Admin/Categories");
        }

        public IActionResult EditCategory(int id, string name)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            Category categoryToEdit = _context.Categories.Single(c => c.ID == id);
            categoryToEdit.Name = name;

            _context.Update(categoryToEdit);
            _context.SaveChanges();
            return Redirect("/Admin/Categories");
        }
        #endregion

        public IActionResult Welcome()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            ViewBag.Name = HttpContext.Session.GetString("fullName");
            return View();
        }


        #region Products

        public IActionResult EditProducts()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            List<Product> products = _context.Products.ToList();
            List<Category> categories = _context.Categories.Where(cat => !cat.IsDeleted).ToList();

            ViewBag.Products = products;
            ViewBag.Categories = categories;
            return View();
        }

        public IActionResult RemoveProduct(int id)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            _context.Products.Single(p => p.ID == id).IsDeleted = true;

            try
            {
                _context.SaveChanges();
            }
            catch (Exception)
            {
                TempData["ProductRemovalFailed"] = true;
            }

            return Redirect("/Admin/EditProducts");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddProduct(string name, int category, int price, string img)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            Product newProduct = new Product()
            {
                Name = name,
                Category = _context.Categories.Single(c => c.ID == category),
                Price = price,
                ImgPath = img
            };

            _context.Add(newProduct);
            await _context.SaveChangesAsync();
            return Redirect("/Admin/EditProducts");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProduct(int id,
                                                     string name,
                                                     int category,
                                                     int price,
                                                     string img)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            Product productToEdit = _context.Products.Single(p => p.ID == id);
            productToEdit.Name = name;
            productToEdit.Category = _context.Categories.Single(c => c.ID == category);
            productToEdit.Price = price;

            if (img != null)
            {
                productToEdit.ImgPath = img;
            }

            _context.Update(productToEdit);
            await _context.SaveChangesAsync();
            return Redirect("/Admin/EditProducts");
        }

        #endregion

        #region Branches

        public IActionResult Branches()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            List<Branch> branches = _context.Branches.Include("AddressInfo").ToList();
            ViewBag.Branches = branches;
            return View();
        }

        public IActionResult RemoveBranch(int id)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            _context.Remove(_context.Branches.Single(b => b.ID == id));
            _context.SaveChanges();
            return Redirect("/Admin/Branches");
        }

        public IActionResult EditBranch(int id, string name, string address, float x, float y)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            Branch branchToEdit = _context.Branches.Single(b => b.ID == id);

            branchToEdit.BranchName = name;
            branchToEdit.AddressInfo = new BranchAddress()
            {
                AddressString = address,
                Latitude = x,
                Longitude = y
            };

            _context.Update(branchToEdit);
            _context.SaveChanges();
            return Redirect("/Admin/Branches");
        }

        public IActionResult AddBranch(int id, string name, string address, float x, float y)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            Branch branchToEdit = new Branch()
            {
                BranchName = name,
                AddressInfo = new BranchAddress()
                {
                    AddressString = address,
                    Latitude = x,
                    Longitude = y
                }
            };

            _context.Add(branchToEdit);
            _context.SaveChanges();
            return Redirect("/Admin/Branches");
        }

        #endregion

        #region Admins

        public IActionResult Admins()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            List<Admin> admins = _context.Admins.ToList();
            ViewBag.Admins = admins;
            ViewBag.SelfAdminId = HttpContext.Session.GetInt32("adminId");
            return View();
        }

        public IActionResult RemoveAdmin(int id)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            _context.Remove(_context.Admins.Single(b => b.Id == id));
            _context.SaveChanges();
            return Redirect("/Admin/Admins");
        }

        public IActionResult EditAdmin(int id, string fullName, string email, string password)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            Admin adminToEdit = _context.Admins.Single(b => b.Id == id);

            adminToEdit.FullName = fullName;
            adminToEdit.Email = email;
            adminToEdit.Password = password;

            _context.Update(adminToEdit);
            _context.SaveChanges();
            return Redirect("/Admin/Admins");
        }

        public IActionResult AddAdmin(int id, string fullName, string email, string password)
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }
            Admin branchToAdd = new Admin()
            {
                FullName = fullName,
                Email = email,
                Password= password
            };

            _context.Add(branchToAdd);
            _context.SaveChangesAsync();
            return Redirect("/Admin/Admins");
        }

        #endregion

        public IActionResult Costumers()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            List<Costumer> costumers = _context.Costumers.ToList();
            List<Order> orders = _context.Orders.ToList();
            List<CostumerViewModel> costumerView = orders.GroupBy(g => g.Costumer.Email)
                .Select(g =>
                {
                    var costumer = costumers.Single(c => c.Email == g.Key);
                    return new CostumerViewModel
                    {
                        FullName = $"{costumer.FirstName} {costumer.LastName}",
                        Email = costumer.Email,
                        OrdersNumber = g.Count()
                    };
                }).ToList();

            ViewBag.CostumersView = costumerView;
            return View();
        }

        public IActionResult Statistics()
        {
            if (HttpContext.Session.GetInt32("adminId") == null)
            {
                return View("Views/Users/PageNotFound.cshtml");
            }

            return View();
        }
    }
}
