﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using final_project.Data;
using final_project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace final_project.Controllers
{
    public class CartController : Controller
    {
        private readonly BookStoreContext _context;

        public CartController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Cart()
        {
            var keys = HttpContext.Session.Keys.Where(key => key != "adminId" && key != "fullName");
            Dictionary<int, int> quantites = new Dictionary<int, int>();
            List<Product> productsInBag = _context.Products.Where(x => keys.Contains(x.ID.ToString())).Include(product=>product.Category).ToList();
            ViewBag.productsInBag = productsInBag;

            foreach (var key in keys)
            {
                quantites.Add(int.Parse(key), int.Parse(HttpContext.Session.GetString(key)));
            }

            ViewBag.quantites = quantites;

            return View();
        }

        [HttpPost]
        public IActionResult AddToCart(int id, int quantity)
        {
            if(HttpContext.Session.GetString(id.ToString()) ==null)
            {
                HttpContext.Session.SetString(id.ToString(), quantity.ToString());
            }
            else
            {
                var newQuantity = int.Parse(HttpContext.Session.GetString(id.ToString())) + quantity;
                HttpContext.Session.SetString(id.ToString(), newQuantity.ToString());
            }
            return RedirectToAction("Shop", "Home");
        }
              
        public IActionResult DeleteProduct(int id)
        {
            HttpContext.Session.Remove(id.ToString());
            return RedirectToAction("Cart", "Cart");
        }

        public IEnumerable<Product> GetCartFormSession()
        {
            var cartIDs = HttpContext.Session.Keys.Where(id => int.TryParse(id, out var num)).Select(int.Parse);
            return _context.Products.Include(prod => prod.Category).Where(product => cartIDs.Contains(product.ID));
        }
    }
}
