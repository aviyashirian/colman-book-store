﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace final_project.Models
{
    public class BranchAddress
    {
        [Key]
        public int Id { get; set; }
        public string AddressString { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
