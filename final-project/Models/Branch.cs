﻿using System.ComponentModel.DataAnnotations;

namespace final_project.Models
{
    public class Branch
    {
        [Key]
        public int ID { get; set; }
        public string BranchName { get; set; }
        public BranchAddress AddressInfo { get; set; }
    }
}
