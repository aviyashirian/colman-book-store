﻿let infobox;

function GetMap() {
    const map = new Microsoft.Maps.Map('#map', {
        credentials: 'ApxKkcD7MByeyxSCRDPy27su9HDczhlYFmI96NQDgpTtdHXdNkLr2922DcqjyKad',
        center: new Microsoft.Maps.Location(32.07511, 34.80000),
        mapTypeId: Microsoft.Maps.MapTypeId.road,
        zoom: 13
    });

    infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
        visible: false
    });

    infobox.setMap(map);
    
    $.ajax({
        type: "GET",
        url: '/Branch/getBranches',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFunc,
        error: errorFunc
    })

    function errorFunc() {
        alert("is it too late now to say sorry?");
    }

    function successFunc(branches) {
        for (var key in branches) {
            let pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(branches[key].addressInfo.latitude, branches[key].addressInfo.longitude));

            pin.metadata = {
                title: branches[key].branchName,
                description: branches[key].addressInfo.addressString
            };

            Microsoft.Maps.Events.addHandler(pin, 'click', pushpinClicked);

            map.entities.push(pin);
        }
    }
}

function pushpinClicked(e) {
    infobox.setOptions({
        location: e.target.getLocation(),
        title: e.target.metadata.title,
        description: e.target.metadata.description,
        visible: true
    });
}